<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctstaff\acp;

/**
* @package module_install
*/
class acp_dctstaff_info
{
	function module()
	{
		return [
			'filename'	=> '\ady\dctstaff\acp\acp_dctstaff_module',
			'title'		=> 'ACP_CAT_DCTSTAFF',
			'modes'		=> [
				'settings'	=> [
                    'title' => 'ACP_DCTSTAFF_CONFIG',
                    'auth'	=> 'ext_ady/dctstaff',
                    'cat'	=> ['ACP_CAT_DCTSTAFF']
                ],
            ],
        ];
	}
}
