<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctstaff\acp;

class acp_dctstaff_module
{
	public $tpl_name;
	public $page_title;
	public $u_action;

	public function main($id, $mode)
	{
		global $phpbb_container;

		// // Add the ACP lang file
		$language = $phpbb_container->get('language');
		$language->add_lang('acp/groups');
		$language->add_lang(['info_acp_dctstaff', 'common'], 'ady/dctstaff');

		// // Set template
		$this->tpl_name = 'acp_dctstaff_' . strtolower($mode);
		$this->page_title = 'DCTSTAFF_ACP_' . strtoupper($mode) . '_TITLE';

		// // Get an instance of the ACP controller and display the options
		$controller = $phpbb_container->get('ady.dctstaff.acp.controller');
		$controller->$mode($this->u_action);
	}
}
