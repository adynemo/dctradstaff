<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctstaff\constant;

class config
{
    const NAME = 'dctstaff_group';
    const USER_COLUMN = 'user_dctstaff_jobs';
}
