<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctstaff\constant;

class jobs
{
    const TRAD = 1;
    const C1 = 2;
    const EDIT = 4;
    const C2 = 8;
}
