<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctstaff\controller;

use ady\dctstaff\constant\config;
use phpbb\db\driver\driver_interface;
use phpbb\group\helper;
use phpbb\language\language;
use phpbb\log\log_interface;
use phpbb\request\request_interface;
use phpbb\template\template;
use phpbb\user;

class acp_controller
{
	/** @var template */
	protected $template;

	/** @var log_interface */
	protected $log;

	/** @var user */
	protected $user;

	/** @var language */
	protected $lang;

	/** @var driver_interface */
	protected $db;

	/** @var request_interface */
	protected $request;

	/** @var helper */
	protected $groupHelper;

	/**
	 * Constructor
	 *
	 * @param template				$template
	 * @param log_interface			$log
	 * @param user					$user
	 * @param language				$lang
	 * @param driver_interface			$db
	 * @param request_interface		$request
	 * @param helper             	$groupHelper
	 */
	public function __construct(
		template $template,
		log_interface $log,
		user $user,
		language $lang,
		driver_interface $db,
		request_interface $request,
        helper $groupHelper
	)
	{
		$this->template    = $template;
		$this->log         = $log;
		$this->user        = $user;
		$this->lang        = $lang;
		$this->db          = $db;
		$this->request     = $request;
		$this->groupHelper = $groupHelper;
	}

	/**
	 * Display the options the admin can configure for this extension
	 *
	 * @param string $u_action
	 */
	public function settings($u_action)
	{
        add_form_key('acp_dctstaff');

        if ($this->request->is_set_post('submit')) {
            if (!check_form_key('acp_dctstaff')) {
                trigger_error($this->lang->lang('FORM_INVALID') . adm_back_link($u_action), E_USER_WARNING);
            }

            $group = $this->request->variable('dctstaff_group', 0);

            if ($group) {
                $sql = 'UPDATE ' . CONFIG_TABLE .
                    ' SET ' . $this->db->sql_build_array('UPDATE', ['config_value' => $group]) .
                    ' WHERE ' . $this->db->sql_build_array('SELECT', ['config_name' => config::NAME]);

                if (!$this->db->sql_query($sql)) {
                    $this->log->add('admin', $this->user->data['user_id'], $this->user->ip, 'LOG_DCTSTAFF_CONFIG_UPDATE_FAILS', false, [$this->user->data['username']]);
                    trigger_error($this->lang->lang('DCTSTAFF_UPDATE_CONFIG_FAILS') . adm_back_link($u_action), E_USER_WARNING);
                }

				$this->log->add('admin', $this->user->data['user_id'], $this->user->ip, 'LOG_DCTSTAFF_CONFIG_UPDATE', false, [$this->user->data['username']]);

                trigger_error($this->lang->lang('DCTSTAFF_UPDATE_CONFIG') . adm_back_link($u_action));
            }
        }

        $sql    = 'SELECT group_id, group_name FROM ' . GROUPS_TABLE;
        $result = $this->db->sql_query($sql);
        $rows   = $this->db->sql_fetchrowset($result);

        $groups = [];
        foreach ($rows as $row) {
            $groups[$row['group_id']] = $this->groupHelper->get_name($row['group_name']);
        }

        $sql    = 'SELECT config_value FROM ' . CONFIG_TABLE . ' WHERE ' . $this->db->sql_build_array('SELECT', ['config_name' => config::NAME]);
        $result = $this->db->sql_query($sql);
        $row    = $this->db->sql_fetchrowset($result);
        $group  = $row[0]['config_value'];

        $this->db->sql_freeresult($result);

        $template_data = [
            'GROUPS'        => $groups,
            'DEFAULT_VALUE' => $group,
            'U_ACTION'      => $u_action,
        ];

        $this->template->assign_vars($template_data);
	}
}
