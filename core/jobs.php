<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctstaff\core;

use ady\dctstaff\constant\jobs as constant;
use phpbb\language\language;

class jobs
{

    public function __construct(language $lang)
    {
        $this->lang = $lang;
        $this->lang->add_lang('common', 'ady/dctstaff');
    }

    public function getJobsLabel()
    {
        return [
            constant::TRAD => $this->lang->lang('DCTSTAFF_TRAD'),
            constant::C1 => $this->lang->lang('DCTSTAFF_C1'),
            constant::EDIT => $this->lang->lang('DCTSTAFF_EDIT'),
            constant::C2 => $this->lang->lang('DCTSTAFF_C2')
        ];
    }
}
