<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctstaff\event;

use ady\dctstaff\constant\config;
use ady\dctstaff\constant\jobs as constant;
use ady\dctstaff\core\jobs;
use phpbb\db\driver\driver_interface;
use phpbb\language\language;
use phpbb\path_helper;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class listener implements EventSubscriberInterface
{
    const JOBS = [
        'trad' => constant::TRAD,
        'c1'   => constant::C1,
        'edit' => constant::EDIT,
        'c2'   => constant::C2
    ];

    /**
     * Assign functions defined in this class to event listeners in the core
     *
     * @return array
     * @static
     * @access public
     */
    static public function getSubscribedEvents()
    {
        return array(
            "core.acp_users_modify_profile"             => "prefill_user_jobs",
            "core.acp_users_profile_validate"           => "define_user_jobs",
            "core.acp_users_profile_modify_sql_ary"     => "modify_sql_ary",
            "core.memberlist_team_modify_query"         => "memberlist_team_modify_query",
            "core.memberlist_team_modify_template_vars" => "memberlist_team_modify_template_vars",
            "core.group_delete_user_after"              => "remove_jobs_after_user_leave_group"
        );
    }

    /** @var driver_interface */
    protected $db;

    /** @var template */
    protected $template;

    /** @var path_helper */
    protected $path_helper;

    /** @var user */
    protected $user;

    /** @var request */
    protected $request;

    /** @var language */
    protected $lang;

    /** @var jobs */
    protected $jobs;

    /** @var string table_prefix */
    protected $table_prefix;

    /** @var string table_prefix */
    protected $phpbb_root_path;

    const MAX_SIZE = 30; // Max size img
    /**
     * Constructor
     */
    public function __construct(
        driver_interface $db,
        template         $template,
        path_helper      $path_helper,
        user             $user,
        request          $request,
        language         $lang,
        jobs             $jobs,
        $table_prefix,
        $phpbb_root_path
    ) {
        $this->db              = $db;
        $this->template        = $template;
        $this->path_helper     = $path_helper;
        $this->user            = $user;
        $this->request         = $request;
        $this->lang            = $lang;
        $this->jobs            = $jobs;
        $this->table_prefix    = $table_prefix;
        $this->phpbb_root_path = $phpbb_root_path;
    }

    /**
     * Adds the template variables for memberlist view profile
     */
    public function prefill_user_jobs($event)
    {
        $sql = 'SELECT ' . config::USER_COLUMN . ' FROM ' . USERS_TABLE . ' WHERE ' . $this->db->sql_build_array('SELECT', ['user_id' => $event['user_id']]);
        $result = $this->db->sql_query($sql);
        $row    = $this->db->sql_fetchrowset($result);
        $this->db->sql_freeresult($result);

        $jobsSum = $row[0][config::USER_COLUMN];
        $jobs = $this->convertJobsSumToJobs($jobsSum);

        $event['data'] = array_merge($event['data'], $jobs);


        $this->template->assign_vars([
            'ADYDCTSTAFF_JOBS' => $jobs,
            'ADYDCTSTAFF_JOBS_LABEL' => $this->jobs->getJobsLabel()
        ]);
    }

    /**
     * Adds the template variables for memberlist view profile
     */
    public function define_user_jobs($event)
    {
        $jobs = $this->request->variable('dctstaff', ['' => 0]);
        $jobsSum = array_sum($jobs);
        $event['data'] = array_merge($event['data'], ['dctstaff_jobs' => $jobsSum]);
    }

    /**
     * Adds the template variables for memberlist view profile
     */
    public function modify_sql_ary($event)
    {
        $event['sql_ary'] = array_merge($event['sql_ary'], [config::USER_COLUMN => $event['data']['dctstaff_jobs']]);
    }

    public function memberlist_team_modify_query($event)
    {
		$sql_ary = $event['sql_ary'];

		$sql_ary['SELECT'] .= ", u." . config::USER_COLUMN;

        $event['sql_ary'] = $sql_ary;
	}

	public function memberlist_team_modify_template_vars($event)
	{
        $row = $event['row'];
        if (!empty($row[config::USER_COLUMN])) {
            $jobs = [];
            foreach ($this->jobs->getJobsLabel() as $job => $label) {
                if ($row[config::USER_COLUMN] & $job) {
                    $jobs[] = $label;
                }
            }
            $template_vars = $event['template_vars'];
            $template_vars['DCTSTAFF_JOBS'] = $jobs;
            $event['template_vars'] = $template_vars;
        }
    }

    public function remove_jobs_after_user_leave_group($event)
    {
        $groupId = $event['group_id'];

        $sql = 'SELECT config_value FROM ' . CONFIG_TABLE . ' WHERE ' . $this->db->sql_build_array('SELECT', ['config_name' => config::NAME]);
        $result = $this->db->sql_query($sql);
        $row    = $this->db->sql_fetchrowset($result);
        $this->db->sql_freeresult($result);

        if ($groupId == $row[0]['config_value']) {
            $sql = 'UPDATE ' . USERS_TABLE .
                ' SET ' . $this->db->sql_build_array('UPDATE', [config::USER_COLUMN => 0]) .
                ' WHERE ' . $this->db->sql_build_array('SELECT', ['user_id' => $event['user_id_ary'][0]]);

            if (!$this->db->sql_query($sql)) {
                trigger_error($this->lang->lang('DCTSTAFF_REMOVE_JOBS_FAILS'), E_USER_WARNING);
            }
        }
    }

    private function convertJobsSumToJobs(int $jobsSum): array
    {
        $output = [];

        foreach (self::JOBS as $jobName=>$jobValue) {
            if ($jobsSum & $jobValue) {
                $output[$jobName] = $this->request->variable('dctstaff_jobs[' . $jobName . ']', $jobValue);
            }
        }

        return $output;
    }
}
