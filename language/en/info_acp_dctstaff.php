<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters for use
// ’ » “ ” …

$lang = array_merge($lang, [
    'DCTSTAFF_ACP_SETTINGS_TITLE'      => 'DC-Trad Staff Configuration',
    'ACP_CAT_DCTSTAFF'                 => 'DC-Trad Staff Configuration',
    'ACP_DCTSTAFF_CONFIG'              => 'Configuration',
    'ACP_DCTSTAFF_DESCRIPTION'         => 'DC-Trad Staff extension configuration',
    'ACP_DCTSTAFF_GROUP_LABEL'         => 'Group',
    'ACP_DCTSTAFF_GROUP_EXPLAIN'       => 'Please choose the concerned group.<br>When a user will leave this group, his jobs will be automatically removed.',
    'ACP_DCTSTAFF_CHOOSE_GROUP'        => 'Choose a group',
    'DCTSTAFF_UPDATE_CONFIG_FAILS'     => 'Update DC-Trad Staff configuration failed.',
    'DCTSTAFF_UPDATE_CONFIG'           => 'DC-Trad Staff configuration has been successfully updated.',
    'LOG_DCTSTAFF_CONFIG_UPDATE'       => '<strong>DC-Trad Staff configuration updated</strong><br>» %1$s',
    'LOG_DCTSTAFF_CONFIG_UPDATE_FAILS' => '<strong>Update DC-Trad Staff configuration failed</strong><br>» %1$s',
    'DCTSTAFF_REMOVE_JOBS_FAILS'       => 'Remove jobs for this user failed',
]);
