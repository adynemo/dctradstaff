<?php

/**
 *
 * @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters for use
// ’ » “ ” …

$lang = array_merge($lang, [
    'DCTSTAFF_ACP_SETTINGS_TITLE'      => 'DC-Trad Staff Configuration',
    'ACP_CAT_DCTSTAFF'                 => 'DC-Trad Staff Configuration',
    'ACP_DCTSTAFF_CONFIG'              => 'Configuration',
    'ACP_DCTSTAFF_DESCRIPTION'         => 'Configuration de l\'extension DC-Trad Staff',
    'ACP_DCTSTAFF_GROUP_LABEL'         => 'Groupe',
    'ACP_DCTSTAFF_GROUP_EXPLAIN'       => 'Veuillez choisir le groupe concerné.<br>Lorsqu\'un utilisateur quittera ce groupe, les métiers lui seront automatiquement retirés.',
    'ACP_DCTSTAFF_CHOOSE_GROUP'        => 'Choisissez un groupe',
    'DCTSTAFF_UPDATE_CONFIG_FAILS'     => 'La mise à jour de la configuration de DC-Trad Staff a échoué.',
    'DCTSTAFF_UPDATE_CONFIG'           => 'La mise à jour de la configuration de DC-Trad Staff a été effectuée avec succès.',
    'LOG_DCTSTAFF_CONFIG_UPDATE'       => '<strong>Mise à jour de la configuration de DC-Trad Staff</strong><br>» %1$s',
    'LOG_DCTSTAFF_CONFIG_UPDATE_FAILS' => '<strong>La mise à jour de la configuration de DC-Trad Staff a rencontré un problème</strong><br>» %1$s',
    'DCTSTAFF_REMOVE_JOBS_FAILS'       => 'Les métiers de cet utilisateur n\'ont pas été retirés',
]);
