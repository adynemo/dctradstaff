<?php

/**
*
* @package phpBB Extension - DC-Trad Staff
 * @copyright (c) 2020 Ady - https://gitlab.com/adynemo
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace ady\dctstaff\migrations;

class dctstaff_1_0_0 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_columns'	=> [
				USERS_TABLE => [
					'user_dctstaff_jobs'	=> ['UINT', 0]
				],
            ]
        ];
	}

	public function revert_schema()
	{
		return [
			'drop_columns' => [
				USERS_TABLE => [
					'user_dctstaff_jobs'
				],
			]
		];
	}

	public function update_data()
	{
		return [
			['config.add', ['dctstaff_group', 0]],
			['module.add', [
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_CAT_DCTSTAFF'
			]],
			['module.add', [
				'acp',
				'ACP_CAT_DCTSTAFF',
				[
					'module_basename'	=> '\ady\dctstaff\acp\acp_dctstaff_module',
					'modes'				=> ['settings'],
				],
			]],
		];
	}
}
